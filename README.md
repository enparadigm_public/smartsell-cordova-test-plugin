




# Smartsell Cordova Plugin

## Installation
```sh
$ cd CORDOVA_PROJECT
$ cordova plugin add https://gitlab.com/enparadigm_public/smartsell-cordova-test-plugin
```
**Note:** The plugin which we are using is present in private repository. So you need to add following lines in your gradle.properties file.
```
artifactory_repo_smart_sell_url=http://artifactory.enparadigm.com/artifactory/smartsell-sdk
artifactory_username=USERNAME_GIVEN_BY_US
artifactory_password=PASSWORD_GIVEN_BY_US
```


### Handling PushNotifiction
Our Library can handle the push notification for you. To make it happen we need to add following in your ``onMessageReceived`` function of ``FirebaseMessagingService``
```kotlin
@Override  
public void onMessageReceived(RemoteMessage remoteMessage) {  
  super.onMessageReceived(remoteMessage);  
  SmartSell.handleFcmNotification(getApplicationContext(), remoteMessage);  
}
```

Also make sure to initialize our library in your Application class. If we are not calling init in your Application class, then the app will crash on receving push Notification.
```kotlin
// Import these packages  
import com.enparadigm.smartsell.SmartSell;  
import com.enparadigm.smartsell.SmartSellConfig;


@Override  
public void onCreate() {  
  //Add this code in your Application class
  //-------START----------  
  SmartSellPluginConfig pluginConfig = SmartSellPluginConfig.getInstance(this);  
  if (pluginConfig.base_url != null && pluginConfig.base_url.length() > 10) {  
    SmartSellConfig config = new SmartSellConfig(pluginConfig.base_url);         
    config.setSignatureFromUserData(pluginConfig.name, pluginConfig.designation, pluginConfig.email, pluginConfig.mobile);  
    config.setLanguage(pluginConfig.language);  
    SmartSell.init(this, config);  
  }
  //-------END-----------
  super.onCreate();  
}
```

## Plugin functions
There are 5 functions in the plugin,

 1. **initialize** with "URL" will initialize the plugin.
 2. **validateUser** will validate the user. Once the user validated successfully you can access function 3, 4 and 5.
 3. **openHomePage** will open the Home page from here you can access all plugin features.
 4. **openDirectory** will open the directory page.
 5. **openPOTD** will open "Poster of the day".

**Note:** If you want to show all the features of the plugin using single entry then use "openHomePage".  This page contaings the link to all the features of the plugin. 

### Sample code to call plugin function
```javascript
/* Make sure to pass the parameter in the following order "API_URL_FOR_PLUGIN,MOBILE_NUMBER,NAME,EMAIL,DESIGNATION,LANGUAGE_SHORT_FORM,USER_TYPE" for this function
  API_URL_FOR_PLUGIN - URL to which the plugin should connect
  LANGUAGE_SHORT_FORM[send "en" for English] - plugin langugae.  eg: If you want plugin in Hindi language then send "hi", 
  USER_TYPE [send "1" for default]
*/
function init() {
  var smartSellPlugin = new SmartSellPlugin();
  smartSellPlugin.initialize("http://dev.enparadigm.com/starhealth_dev_api/public/index.php/v1/","9999999999","UsersName","email@mail.com","Developer","en","1",function (msg) {
              console.log(msg);
        },
        function (err) {
              console.log(err);
        });
}

// For "validateUser","openHomePage","openDirectory" and "openPOTD" we don\'t need to pass parameters
function validate(){
  var smartSellPlugin = new SmartSellPlugin();
  smartSellPlugin.validateUser(function (msg) {
    console.log(msg);
  },
  function (err) {
    console.log(err);
  });
}
```