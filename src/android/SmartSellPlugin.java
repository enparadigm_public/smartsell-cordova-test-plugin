package com.enparadigm.smartsellplugin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.enparadigm.smartsell.LoginTask;
import com.enparadigm.smartsell.SmartSell;
import com.enparadigm.smartsell.SmartSellConfig;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * This class echoes a string called from JavaScript.
 */
public class SmartSellPlugin extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("initialize") && args.length() == 7) {
            initSmartSell(args, callbackContext);
            return true;
        } else if (action.equalsIgnoreCase("validateUser")) {
            validateUser(callbackContext);
            return true;
        } else if (action.equalsIgnoreCase("openDirectory")) {
            openDirectory(callbackContext);
            return true;
        } else if (action.equalsIgnoreCase("openPOTD")) {
            openPOTD(callbackContext);
            return true;
        } else if (action.equalsIgnoreCase("openHomePage")) {
            openHomePage(callbackContext);
            return true;
        }else if (action.equals("startActivity")) {
            Toast.makeText(cordova.getContext(), "startActivity", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    private void initSmartSell(JSONArray args, CallbackContext callbackContext) {
        try {
            Context context = cordova.getActivity().getApplication();
            SmartSellPluginConfig.setBaseURL(context, args.getString(0));
            SmartSellPluginConfig.setNumber(context, args.getString(1));
            SmartSellPluginConfig.setName(context, args.getString(2));
            SmartSellPluginConfig.setEmail(context, args.getString(3));
            SmartSellPluginConfig.setDesignation(context, args.getString(4));
            SmartSellPluginConfig.setLanguage(context, args.getString(5));
            SmartSellPluginConfig.setUserType(context, args.getString(6));

            SmartSellPluginConfig pluginConfig = SmartSellPluginConfig.getInstance(context);
            SmartSellConfig config = new SmartSellConfig(pluginConfig.base_url);
            config.setSignatureFromUserData(pluginConfig.name, pluginConfig.designation, pluginConfig.email, pluginConfig.mobile);
            config.setLanguage(pluginConfig.language);
            SmartSell.init(cordova.getActivity().getApplication(), config);
            callbackContext.success("Success");
        } catch (Exception e) {
            if (e.getMessage().equalsIgnoreCase("Koin is already started. Run startKoin only once or use loadKoinModules")) {
                callbackContext.success("Success");
            } else {
                callbackContext.error("Exception : " + e.getMessage());
                Toast.makeText(cordova.getContext(), "initialize Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void validateUser(CallbackContext callbackContext) {
        SmartSellPluginConfig pluginConfig = SmartSellPluginConfig.getInstance(cordova.getActivity().getApplication());
        SmartSell.validateUser(cordova.getActivity(), pluginConfig.mobile, pluginConfig.userType, new LoginTask() {
            @Override
            public void success() {
                callbackContext.success("Success");
            }

            @Override
            public void failure(Throwable error) {
                callbackContext.error("Exception : " + error.getMessage());
                Toast.makeText(cordova.getActivity(), "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openDirectory(CallbackContext callbackContext) {
        SmartSell.openDirectories(cordova.getActivity());
        callbackContext.success("Success");
    }

    private void openPOTD(CallbackContext callbackContext) {
        SmartSell.openPosterOfTheDay(cordova.getActivity());
        callbackContext.success("Success");
    }

    private void openHomePage(CallbackContext callbackContext) {
        SmartSell.openHomePage(cordova.getActivity());
        callbackContext.success("Success");
    }
    
}
