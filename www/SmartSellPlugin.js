var exec = cordova.require('cordova/exec');
var PLUGIN_NAME = "SmartSellPlugin";
var SmartSellPlugin = function() {
    console.log('SmartSellPlugin instanced');
};

SmartSellPlugin.prototype.initialize = function(baseUrl,mobile,name,email,designation,language,userType,onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    exec(successCallback, errorCallback, PLUGIN_NAME, 'initialize', [baseUrl,mobile,name,email,designation,language,userType]);
};

SmartSellPlugin.prototype.validateUser = function(onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    var argument = [];
    console.log('Executed validateUser');
    exec(successCallback, errorCallback, PLUGIN_NAME, 'validateUser', argument);
};

SmartSellPlugin.prototype.openDirectory = function(onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    var argument = [];
    console.log('Executed openDirectory');
    exec(successCallback, errorCallback, PLUGIN_NAME, 'openDirectory', argument);
};

SmartSellPlugin.prototype.openPOTD = function(onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    var argument = [];
    console.log('Executed openPOTD');
    exec(successCallback, errorCallback, PLUGIN_NAME, 'openPOTD', argument);
};
SmartSellPlugin.prototype.openHomePage = function(onSuccess, onError) {
    var errorCallback = function(obj) {
        onError(obj);
    };

    var successCallback = function(obj) {
        onSuccess(obj);
    };
    var argument = [];
    console.log('Executed openHomePage');
    exec(successCallback, errorCallback, PLUGIN_NAME, 'openHomePage', argument);
};



if (typeof module != 'undefined' && module.exports) {
    module.exports = SmartSellPlugin;
}